jQuery(function ($) {

    $(document).ready(function () {
        $(".js-select2").each(function (i, select) {
            var id = $(select).attr('id');
            var data = [];

            $(select).find("option").each(function (i, option) {
                var id = $(option).val();
                var value = $(option).text();
                data.push({id: id, text: value});
            });


            function formatIcon(icon) {

                var $icon = $(
                    '<div class="select2-choice-with-icon"> <span class="' + icon.id + '"></span> <span>' + icon.text + '</span></div>'
                );
                return $icon;
            };

            $("#js-select2-" + id).select2({
                createSearchChoice: function (term, data) {

                    if ($(data).filter(function () {
                            return this.text.localeCompare(term) === 0;
                        }).length === 0) {
                        return {id: term, text: term};
                    }

                },
                initSelection: function (element, callback) {

                    callback({id: element.val(), text: $('#'+element.val()).text()});
                },
                containerCssClass: "select2-icon-container",
                placeholder: "Choose a value",
                formatResult: formatIcon,
                formatSelection: formatIcon,
                width: "100%",
                data: data
            });
        });
    })
});