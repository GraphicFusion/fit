;jQuery(document).ready(function ($) {
    if ($('.map-canvas').length) {
        var maps = {};
        jQuery.each(contact_maps, function (index, block_map) {
            var mapCenter = new google.maps.LatLng(block_map.center_lat, block_map.center_lng);
            var mapOptions = {
                zoom: block_map.zoom,
                center: mapCenter,
                scrollwheel: false,
                streetViewControl: false,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
            };
            var map_id = 'map-canvas-' + block_map.map_count;
            var map_name = 'map_' + block_map.map_count;
            maps[map_name] = new google.maps.Map(document.getElementById(map_id), mapOptions);
            var marker, i;
            window.infoWindow = new google.maps.InfoWindow({
                pixelOffset: new google.maps.Size(0, 0) // accounts for the offset of the marker when it is resized
            });

            loadMarkers(this_map.markers, block_map.map_count);
            // STYLE MAP

            var styles = [
                {
                    "featureType": "all",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "saturation": 36
                        },
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 40
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        },
                        {
                            "weight": 1.2
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 29
                        },
                        {
                            "weight": 0.2
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 18
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 19
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                }
            ];
            maps[map_name].setOptions({styles: styles});
        });
    }
    /* loadMarkers()
     *
     * iterates over all the markers passed in content-map.php
     * if the category passed matches the marker category, the marker is built
     */
    function loadMarkers(markers, map_id) {
        for (i = 0; i < markers.length; i++) {
            var $title = markers[i]['name'],
                $address = markers[i]['address'],
                $phone = "",
                $email = "",
                position = new google.maps.LatLng(markers[i]['lat'], markers[i]['lng']),
                $icon = markers[i]['icon'];
            // icons are defined in acf fields of the map category; if it is undefined, will use a default marker style
            var image = {
                url: $icon,							// defined at top of script
                size: new google.maps.Size(47, 73),			// pixel size of original png
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0),		// offsets the marker base
                scaledSize: new google.maps.Size(28, 44)	// desired pixel size of png
            };

            var map_name = 'map_' + map_id;
            marker = new google.maps.Marker({
                position: position,
                map: maps[map_name],
                title: markers[i]['name'],
                icon: image
            });
            var $content = '<div id="content"><b>Address: </b><span>' + $address + '</span></div>';
            marker.content = $content;
            marker.map_id = map_id;
            marker.set("parent", $content);
            allMarkers.push(marker);
            // Allow each marker to have an info window 'mouseover' or 'click'
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    /* setup marker descriptions */
                    marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
                    var map_id = marker.map_id;
                    if ('undefined' != marker.content) {
                        window.infoWindow.setContent(marker.content);
                        window.infoWindow.category = marker.category;
                        window.infoWindow.open(maps[map_name], marker);
                    }
                };
            })(marker, i));
        }
    }

    if ($('#map-canvas').length) {
        // will load google map if map-block is showing by default
        if ($('.map-block').is(':visible')) {
            google.maps.event.addDomListener(window, 'load', function (event) {
                initialize(event);
            });
        }
    }
});