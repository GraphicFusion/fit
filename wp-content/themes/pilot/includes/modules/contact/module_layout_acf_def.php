<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = 				array (
					'key' => '570bc1726b28e',
					'name' => 'contact_block',
					'label' => 'Contact Block',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_570bd92468e07',
							'label' => 'Zoom',
							'name' => 'contact_block_zoom',
							'type' => 'number',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => 30,
								'class' => '',
								'id' => '',
							),
							'default_value' => 7,
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => 0,
							'max' => 20,
							'step' => 1,
							'readonly' => 0,
							'disabled' => 0,
						),
						array (
							'key' => 'field_570bd8c545669',
							'label' => 'Map Center',
							'name' => 'contact_block_map_center',
							'type' => 'google_map',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => 70,
								'class' => '',
								'id' => '',
							),
							'center_lat' => '39.5',
							'center_lng' => '-98.35',
							'zoom' => 4,
							'height' => 200,
						)
					),
					'min' => '',
					'max' => '',
				);
?>