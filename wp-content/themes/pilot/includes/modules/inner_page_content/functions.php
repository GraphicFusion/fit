<?php
	$filename = get_template_directory() . '/includes/modules/inner_page_content/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}

	function build_inner_page_content_layout(){
		$args = array(
			'top_content' => get_sub_field('inner_page_top_content'),
			'left_content' => get_sub_field('inner_page_left_content'),
			'right_content' => get_sub_field('inner_page_right_content'),
			'bottom_content' => get_sub_field('inner_page_bottom_content')
		);
		return $args;
	}
?>