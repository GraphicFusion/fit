<?php
global $pilot;

$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if (file_exists($filename)) {
    require $filename;
}

function build_slider_layout()
{
    $args = array(
        'slide_type' => 'slide',
        'title' => get_sub_field('slider_block_title'),
        'slides' => get_sub_field('slider_block_slides')
    );
    return $args;
}

if (in_array('slider', $pilot->modules) && !$pilot->slick_enqueued) {
    add_action('wp_enqueue_scripts', 'enqueue_slick');
}
function enqueue_slick()
{
    global $pilot;
    $pilot->slick_enqueued = 1;
}

?>