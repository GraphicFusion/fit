jQuery(document).ready(function ($) {
    $('.block-slider .slider-block').slick({
        slidesToShow: 1,
        easing: 'swing',
        arrows: true,
        nextArrow: '.slick-next',
        prevArrow: '.slick-prev',
        autoplay: true,
        responsive: [
            {
                breakpoint: 768,
            },
            {
                breakpoint: 480,
            }
        ]
    });
});