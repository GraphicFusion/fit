<?php
	function get_additional_fields( $module ){
		global $pilot;
		$module = preg_replace('/-/','_',$module);
		$additional_fields[] = get_custom_class_field( $module );
		if( $pilot->use_colormaker ){
			$additional_fields[] = get_block_color_fields( $module );
		}		
		return $additional_fields;
	}
	function get_custom_class_field( $module ){
		$field = array (
			'key' => 'field_56c60bf031ab6'.$module,
			'label' => 'Custom Classes',
			'name' => 'custom_classes_'.$module,
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		);
		return $field;
	};

	function get_block_color_fields( $module ){
		$field = array (
			'key' => 'field_568e6c04f59bb'.$module,
			'label' => 'Block Color',
			'name' => $module.'_block_color',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'horizontal',
		);
		return $field;
	}

?>