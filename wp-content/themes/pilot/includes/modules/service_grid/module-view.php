<?php
global $args;
?>

<div class="service-grid-wrap">

    <div class="col-12 text-align-center services-title-container">
        <span class="services-title"><?php echo $args['title'] ?></span>
    </div>
    <div class="services-container">
        <?php foreach ($args['services'] as $idx => $arg) : ?>
            <?php $modifier = ($idx != 0 && $idx % 3 == 0 ) ? 'col-last' : '' ?>

            <div class="col-3 <?php echo $modifier ?>">
                <div class="service-grid-icon">
                    <span class="<?php echo $arg['icon'] ?>"/>
                </div>
                <div class="service-grid-title">
                    <h4><?php echo $arg['column_title'] ?></h4>
                </div>
                <div class="service-grid-content">
                    <?php echo $arg['content'] ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="services-information-container">
        <div class="col-12 text-align-center">
            <?php if ($args['button_href']) : ?>
                <a class="button-default"
                   href="<?php echo $args['button_href']; ?>"><?php echo $args['button_text']; ?></a>
            <?php endif; ?>
        </div>
    </div>
</div>
