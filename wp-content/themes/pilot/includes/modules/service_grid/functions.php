<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if (file_exists($filename)) {
    require $filename;
}

function build_service_grid_layout()
{

    $button_object = (get_sub_field('service_grid_button_href') ? get_sub_field('service_grid_button_href') : get_sub_field('service_grid_custom_button_href'));
    if (is_object($button_object)) {
        $button_href = get_permalink($button_object->ID);
    } else {
        $button_href = $button_object;
    }

    $args = array(
        'button_text' => get_sub_field('service_grid_button_text'),
        'button_href' => $button_href,
        'title' => get_sub_field('service_grid_title'),
        'services' => get_sub_field('service_grid_services')
    );
    return $args;
}

?>