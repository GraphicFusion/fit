jQuery(document).ready(function ($) {
    $('.block-quote_slider .quote-slider-block').slick({
        slidesToShow: 1,
        easing: 'swing',
        arrows: false,
        autoplay: true,
        dots: true
    });
});