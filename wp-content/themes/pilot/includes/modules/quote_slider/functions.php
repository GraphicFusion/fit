<?php
global $pilot;

$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if (file_exists($filename)) {
    require $filename;
}

function build_quote_slider_layout()
{




    $args = array(
        'quotes' => get_sub_field('quote_slider_quotes')
    );
    return $args;
}

?>