<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => 'f3db499430e1',
		'name' => 'quote_slider',
		'label' => 'Quote Slider',
		'display' => 'block',
		'sub_fields' => array (
			array (
				'key' => 'field_f3db499430e2',
				'label' => 'Quotes',
				'name' => 'quote_slider_quotes',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 1,
				'max' => '',
				'layout' => 'table',
				'button_label' => 'Add Quote',
				'sub_fields' => array (
					array (
						'key' => 'field_f3db499430e3',
						'label' => 'Quote',
						'name' => 'quote',
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'thumbnail',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
				),
			),
		),
		'min' => '',
		'max' => '',
	);
?>