<?php
global $args;
?>

<div class="quote-slider-block">



	<?php foreach( $args['quotes'] as $quote ): ?>

		<div class="quote-slider-slide">
			<?php echo $quote['quote']; ?>
		</div>

	<?php endforeach; ?>

</div>