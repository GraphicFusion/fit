<?php
/**
 * string $args[0]['title']
 * string $args[0]['subtitle']
 * string $args[0]['content']
 * string $args[0]['bg_image_url']
 * string $args[0]['overlay_opacity']            // from 0 to 1
 * string $args[0]['overlay_color']                // hex value #010101
 * string $args[0]['column_image_url']
 */
global $args;
?>

<?php if (isset($args['bg_image_url'])) : ?>
    <div class="bg-image" style="background-image: url(<?php echo $args['bg_image_url']; ?>);">
        <div class="img-overlay"
             style="background-color: <?php echo $args['overlay_color']; ?>; opacity: <?php echo $args['overlay_opacity']; ?>;"></div>
    </div>
<?php else : ?>
    <div class="bg-image">
        <div class="img-overlay"
             style="background-color: <?php echo $args['overlay_color']; ?>; opacity: <?php echo $args['overlay_opacity']; ?>;"></div>
    </div>
<?php endif; ?>


<div class="multi-content-wrap">
    <div class="col-6 offset-6 col-last inner-multi-content">
        <div class="title">
            <div class="title-wrap">
                <?php if (isset($args['title'])) : ?>
                    <span class="half-half-title"><?php echo $args['title']; ?></span>
                <?php endif; ?>
                <?php if (isset($args['logo'])) : ?>
                    <img src="<?php echo $args['logo']['url']; ?>">
                <?php endif; ?>
                <span class="subtitle"><?php echo $args['subtitle']; ?></span>
                <div class="content"><?php echo $args['content']; ?></div>
                <div class="half-half-button">
                    <?php if ($args['button_href']) : ?>
                        <?php if ($args['button_href']) : ?>
                            <a class="button-default"
                               href="<?php echo $args['button_href']; ?>"><?php echo $args['button_text']; ?></a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

