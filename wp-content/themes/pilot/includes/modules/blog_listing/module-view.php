<?php
global $args;
?>

<div class="blog-listing-wrap">

    <ul class="blog-list">


        <?php $the_query = new WP_Query('posts_per_page=' . $args['posts_to_show']); ?>

        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>


            <li class="blog-list-item">
                <div class="blog-list-item-inner">
                    <div class="blog-list-item-title">
                        <h3><?php the_title(); ?></h3>
                    </div>
                    <div class="blog-list-item-meta">
                        <small><?php the_time('m/d/Y') ?> By <!-- by <?php the_author() ?> --></small>
                    </div>
                    <div class="blog-list-item-excerpt">
                        <?php the_excerpt(__('(Read more)')); ?>
                    </div>
                    <div class="blog-list-item-footer">
                        <a href="<?php the_permalink() ?>">Read more</a>
                    </div>
                </div>

            </li>
            <?php

        endwhile;

        wp_reset_postdata();

        ?>
    </ul>
    <div class="col-12 text-align-center">
        <?php if ($args['button_href']) : ?>
            <a class="button-default"
               href="<?php echo $args['button_href']; ?>"><?php echo $args['button_text']; ?></a>
        <?php endif; ?>
    </div>


</div>
