<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if (file_exists($filename)) {
    require $filename;
}

function build_blog_listing_layout()
{

    $button_object = (get_sub_field('blog_listing_button_href') ? get_sub_field('blog_listing_button_href') : get_sub_field('blog_listing_custom_button_href'));
    if (is_object($button_object)) {
        $button_href = get_permalink($button_object->ID);
    } else {
        $button_href = $button_object;
    }

    $args = array(
        'button_text' => get_sub_field('blog_listing_button_text'),
        'button_href' => $button_href,
        'posts_to_show' => get_sub_field('blog_listing_amount')
    );
    return $args;
}

?>