jQuery(document).ready(function ($) {
    $('.instagram_block_wrapper .slider-container').slick({
        slidesToShow: 4,
        easing: 'swing',
        arrows: true,
        nextArrow: $('.ig-next'),
        prevArrow: $('.ig-prev'),
        autoplay: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    nextArrow: $('.ig-next'),
                    prevArrow: $('.ig-prev')
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    centerMode: false,
                    nextArrow: $('.ig-next'),
                    prevArrow: $('.ig-prev')
                }
            }
        ]
    });
});