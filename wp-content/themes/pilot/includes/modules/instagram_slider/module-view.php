<?php
/**
 * string    $args['title']
 * array    $args['slides']
 * string    $args['slides'][0]['type']        // define a type in acf; allows us to use the same slider block for multiple data sources
 * string    $args['slides'][0]['date']
 * string    $args['slides'][0]['permalink']
 * string    $args['slides'][0]['author']
 * string    $args['slides'][0]['title']
 * string    $args['slides'][0]['category']
 * array    $args['slides'][0]['image']    // thumbnail array
 * string    $args['slides'][0]['image'][0]    // thumbnail url
 * string    $args['slides'][0]['image'][1]    // thumbnail width px
 * string    $args['slides'][0]['image'][2]    // thumbnail height px
 *
 * slick slider is called from src/js/main.js
 */

global $args;
$type = $args['slide_type'];
?>
<?php if (count($args['slides']) > 0) : ?>
    <div class="block-slider instagram-block-container">
        <h3 class="instagram-title"><?php get_template_part('views/icons/instagram_square'); ?>
            @<?php echo $args['username']; ?></h3>

        <div class="instagram_block_wrapper">
            <div class="instagram-slider-inner">
                <button type="button" data-role="none" class="ig-prev slick-prev slick-arrow" aria-label="Previous"
                        role="button"
                        style="display: block;">
                    <?php get_template_part('views/icons/arrow'); ?>
                </button>
                <div class="slider-container">

                    <?php foreach ($args['slides'] as $slide): ?>
                        <div class="slick-slide">
                            <a href="<?php echo $slide['permalink']; ?>">
                                <div class="bg-image"
                                     style="background-image: url(<?php echo $slide['thumbnail']; ?>)"></div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
                <button type="button" data-role="none" class="ig-next slick-next slick-arrow" aria-label="Next"
                        role="button"
                        style="display: block;">
                    <?php get_template_part('views/icons/arrow'); ?>
                </button>
            </div>

        </div>
        <a class="instagram-link" href="<?php echo $args['link']; ?>">Follow</a>
    </div>
<?php endif; ?>

