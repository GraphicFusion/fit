<?php
$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
if (file_exists($filename)) {
    require $filename;
}
function build_instagram_slider_layout() {
    $username = get_sub_field('instagram_slider_block_username');

    $slides = array();
    $user = null;
    $link = null;

    $obj = null;
    if ($username !== "") {
        try {
            $json = file_get_contents('https://www.instagram.com/' . $username . '/media/');
            $obj = json_decode($json);
        }
        catch (Exception $e) {

        }
    }

    if (isset($obj) && count($obj->items)) {
        $first_photo = $obj->items[0];
        $user = $first_photo->user->username;
        $link = "https://instagram.com/$user";
        foreach ($obj->items as $slide_post) {
            $permalink = $slide_post->link;
            $thumbnail = $slide_post->images->standard_resolution->url;
            $slides[] = array(
                'thumbnail' => $thumbnail,
                'permalink' => $permalink,
            );
        }
    }
    $args = array(
        'title' => get_sub_field('instagram_slider_block_title'),
        'slide_type' => 'slide',
        'slides' => $slides,
        'link' => $link,
        'username' => $user
    );
    return $args;
}

if (in_array('instagram_slider', $pilot->modules) && !$pilot->slick_enqueued) {
    add_action('wp_enqueue_scripts', 'enqueue_slick');
}

function enqueue_instagram_slick() {
    global $pilot;
    $pilot->slick_enqueued = 1;
}

?>