<?php
	global $pilot;
	// add a module layout to be flexible and
	$module_layout = array (
		'key' => '568e6dd57za0f',
		'name' => 'instagram_slider_block',
		'label' => 'Instagram Slider',
		'display' => 'block',
		'sub_fields' => array (
			array (
				'key' => 'field_56951fdc7c2g7',
				'label' => 'User Name',
				'name' => 'instagram_slider_block_username',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			)
		),
		'min' => '',
		'max' => '',
	);
?>