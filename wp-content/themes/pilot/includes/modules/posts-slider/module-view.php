<?php 
	/**
	 * string	$args['title']
	 * array	$args['slides']
	 * string	$args['slides'][0]['type'] 		// define a type in acf; allows us to use the same slider block for multiple data sources
	 * string	$args['slides'][0]['date'] 		
	 * string	$args['slides'][0]['permalink'] 		
	 * string	$args['slides'][0]['author'] 		
	 * string	$args['slides'][0]['title'] 		
	 * string	$args['slides'][0]['category'] 		
	 * array	$args['slides'][0]['image'] 	// thumbnail array
	 * string	$args['slides'][0]['image'][0] 	// thumbnail url 
	 * string	$args['slides'][0]['image'][1] 	// thumbnail width px
	 * string	$args['slides'][0]['image'][2] 	// thumbnail height px
	 *
	 * slick slider is called from src/js/main.js
	 */

	global $args; 
	$type = $args['slide_type'];
?>
<h3><?php echo $args['title']; ?></h3>
<?php if( count($args['slides']) > 0 ) : ?>
	<div class="slider-block">
		<?php foreach( $args['slides'] as $slide ): ?>

			<?php if( 'slide' == $type ) : ?>
				<div class="slide">
					<a href="<?php echo $slide['permalink']; ?>">
						<div class="bg-image" style="background-image: url(<?php echo $slide['thumbnail'][0]; ?>)"></div>
						<div class="slide-text">
							<span class="category"><?php echo $slide['category']; ?></span>
							<span class="title"><?php echo $slide['title']; ?></span>
							<span class="subtitle"><?php echo $slide['date']; ?></span>					
							<span class="author"><?php echo $slide['author']; ?></span>
						</div>
					</a>
				</div>
			<?php endif; ?>

			<?php if( 'image' == $type ) : ?>
				<div class="image-only">
					<div class="bg-image" style="background-image: url(<?php echo $slide['image']['url']; ?>)"></div>
				</div>
			<?php endif; ?>

		<?php endforeach; ?>
	</div>
<?php endif; ?>