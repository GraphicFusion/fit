<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 * array	$args['products']			 			//  array of rows of lede/content
	 * string	$args['products'][0]		 			//  wordpress post object
	 */
	global $args; 

?>
<h3><?php echo $args['title']; ?></h3>
<ul class="coffee-products">
	<?php foreach ( $args['products'] as $product ) : $_product = wc_get_product( $product->ID ); ?>
		<?php 
			$meta_array = get_post_meta($product->ID, '_weight'); 
			$weight = "";
			if( is_array( $meta_array ) ){
				$weight = $meta_array[0];
			}
		?>
		<li>
		    <div class="product-left">
				<span class="product-name"><?php echo $product->post_title; ?></span>
				<a href="<?php echo get_permalink($product->ID); ?>">Details</a>
			</div>
			<div class="product-right">
				<span class="size"><?php echo $weight. " " .get_option('woocommerce_weight_unit' ); ?></span>
				<span class="price"><?php echo $_product->get_price(); ?></span>
				<?php $shortcode = '[add_to_cart id="'.$product->ID.'"]'; echo do_shortcode($shortcode); ?>
			</div>
		</li>
	<?php endforeach; ?>
</ul>
