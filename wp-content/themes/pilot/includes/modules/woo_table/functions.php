<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_woo_table_layout(){
		$args = array(
			'title' => get_sub_field('woo_table_block_title'),
			'products' => get_sub_field('woo_table_block_products')
		);
		return $args;
	}

?>