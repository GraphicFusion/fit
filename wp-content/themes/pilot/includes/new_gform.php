<?php
/*
Plugin Name: Programmatically add Gravity Forms
Plugin URI: https://daan.kortenba.ch/programmatically-add-gravity-forms/
Description: Programmatically add persistent Gravity Forms forms.
Author: Daan Kortenbach
Version: 0.1
Author URI: https://daan.kortenba.ch/
License: GPLv2 or later
*/

class ProgrammaticallyAddGravityForm {

	private $gf_contact_form = array();


	/**
	 * [setForm description]
	 *
	 * @param [type] $gf_contact_form [description]
	 */
	public function setForm( $gf_contact_form ) {

		$this->gf_contact_form = $gf_contact_form;

		add_action( 'admin_head',                 array( $this, 'form_create' ), 999 );
		add_action( 'gform_after_save_form',      array( $this, 'form_overrides' ) );
		add_filter( 'gform_add_field_buttons',    array( $this, 'fs_remove_fields_gui' ), 10, 1 );
		add_filter( 'gform_form_trash_link',      array( $this, 'fs_remove_trash_link' ), 10, 1 );
		add_filter( 'gform_delete_field_link',    array( $this, 'fs_remove_field_link' ), 10, 1 );
		add_filter( 'gform_duplicate_field_link', array( $this, 'fs_remove_duplicate_field_link' ), 10, 1 );
		add_filter( 'gform_toolbar_menu',         array( $this, 'fs_remove_toolbar_menu_items' ), 10, 2 );
		add_filter( 'gform_form_actions',         array( $this, 'fs_remove_form_actions' ), 10, 2 );

	}


	/**
	 * [form_create description]
	 *
	 * @return [type] [description]
	 */
	public function form_create() {

		$forms = RGFormsModel::get_forms( null, 'title' );

		if ( count( $forms ) > 0 ) {

			foreach ( $forms as $form ) {
				if ( $form->title == $this->gf_contact_form['title'] ) {

					$form_exists = true;
				}
			}
		}



		if ( $form_exists != true ) {
			$result = GFAPI::add_form( $this->gf_contact_form );
		}
	}


	/**
	 * [form_overrides description]
	 *
	 * @return [type] [description]
	 */
	public function form_overrides() {


		$form_exists  = false;

		$forms = GFAPI::get_forms();

		if ( count( $forms ) > 0 ) {

			$order = array();

			foreach ( $forms as $form ) {
				if ( $form['title'] == $this->gf_contact_form['title'] ) {

					$form_exists = true;
					$form_id = $form['id'];
					$form_fields = $form['fields'];

					$field_keys = array_keys( $form['fields'] );

					foreach ( $field_keys as $key ) {
						$order[] = $form['fields'][$key]['id'];
					}
				}
			}
		}


		if ( $form_exists == true ) {

			// set form ID
			$this->gf_contact_form['id'] = $form_id;

			// Get field keys
			$field_keys = array_keys( $this->gf_contact_form['fields'] );

			$order_original = array();

			// Get original order
			foreach ( $field_keys as $key ) {
				$order_original[] = $this->gf_contact_form['fields'][$key]['id'];
			}

			// temp set original form fields before overwriting them
			$original_form_fields = $this->gf_contact_form['fields'];


			// flip keys for values
			$order_original = array_flip( $order_original );

			// reorder original form fields
			$count = 0;
			foreach ( $original_form_fields as $key ) {
				// var_dump($form_fields[$count]['id']);
				$this->gf_contact_form['fields'][$count] = $original_form_fields[$order_original[$form_fields[$count]['id']]];

				// user editable fields
				$this->gf_contact_form['fields'][$count]['label']         = $form_fields[$count]['label'];
				$this->gf_contact_form['fields'][$count]['size']          = $form_fields[$count]['size'];
				$this->gf_contact_form['fields'][$count]['description']   = $form_fields[$count]['description'];
				$this->gf_contact_form['fields'][$count]['error_message'] = $form_fields[$count]['error_message'];
				$this->gf_contact_form['fields'][$count]['css_class']     = $form_fields[$count]['css_class'];


				$count++;
			}

			// update form
			$result = GFAPI::update_form( $this->gf_contact_form );
		}
	}


	/**
	 * [fs_remove_fields_gui description]
	 *
	 * @param [type]  $field_groups [description]
	 * @return [type]               [description]
	 */
	public function fs_remove_fields_gui( $field_groups ) {

		if ( isset( $_GET['id'] ) ) {
			$forms = GFAPI::get_forms();

			$id = intval( $_GET['id'] );

			if ( count( $forms ) > 0 ) {
				foreach ( $forms as $form ) {
					if ( $form['title'] == $this->gf_contact_form['title'] && $id == $form['id'] ) {
						$field_groups = array();
					}
				}
			}
		}

		return $field_groups;
	}


	/**
	 * [fs_remove_trash_link description]
	 *
	 * @param [type]  $trash_link [description]
	 * @return [type]             [description]
	 */
	public function fs_remove_trash_link( $trash_link ) {

		if ( isset( $_GET['id'] ) ) {
			$forms = GFAPI::get_forms();

			$id = intval( $_GET['id'] );

			if ( count( $forms ) > 0 ) {
				foreach ( $forms as $form ) {
					if ( $form['title'] == $this->gf_contact_form['title'] && $id == $form['id'] ) {
						$trash_link = '';
					}
				}
			}
		}

		return $trash_link;
	}


	/**
	 * [fs_remove_field_link description]
	 *
	 * @param [type]  $remove_field_link [description]
	 * @return [type]                    [description]
	 */
	public function fs_remove_field_link( $remove_field_link ) {

		if ( isset( $_GET['id'] ) ) {
			$forms = GFAPI::get_forms();

			$id = intval( $_GET['id'] );

			if ( count( $forms ) > 0 ) {
				foreach ( $forms as $form ) {
					if ( $form['title'] == $this->gf_contact_form['title'] && $id == $form['id'] ) {
						$remove_field_link = '';
					}
				}
			}
		}

		return $remove_field_link;
	}


	/**
	 * [fs_remove_duplicate_field_link description]
	 *
	 * @param [type]  $duplicate_field_link [description]
	 * @return [type]                       [description]
	 */
	public function fs_remove_duplicate_field_link( $duplicate_field_link ) {

		if ( isset( $_GET['id'] ) ) {
			$forms = GFAPI::get_forms();

			$id = intval( $_GET['id'] );

			if ( count( $forms ) > 0 ) {
				foreach ( $forms as $form ) {
					if ( $form['title'] == $this->gf_contact_form['title'] && $id == $form['id'] ) {
						$duplicate_field_link = '';
					}
				}
			}
		}

		return $duplicate_field_link;
	}


	/**
	 * [fs_remove_toolbar_menu_items description]
	 *
	 * @param [type]  $menu_items [description]
	 * @param [type]  $id         [description]
	 * @return [type]             [description]
	 */
	public function fs_remove_toolbar_menu_items( $menu_items, $id ) {

		if ( isset( $_GET['id'] ) ) {

			$forms = GFAPI::get_forms();

			$id = intval( $_GET['id'] );

			if ( count( $forms ) > 0 ) {
				foreach ( $forms as $form ) {
					if ( $form['title'] == $this->gf_contact_form['title'] && $id == $form['id'] ) {
						unset( $menu_items['settings'] );
						unset( $menu_items['entries'] );
					}
				}
			}
		}

		return $menu_items;
	}


	/**
	 * [fs_remove_form_actions description]
	 *
	 * @param  [type] $form_actions [description]
	 * @param  [type] $id           [description]
	 * @return [type]               [description]
	 */
	public function fs_remove_form_actions( $form_actions, $id ) {

		$forms = GFAPI::get_forms();

		if ( count( $forms ) > 0 ) {
			foreach ( $forms as $form ) {
				if ( $form['title'] == $this->gf_contact_form['title'] && $id == $form['id'] ) {
					unset( $form_actions['settings'] );
					unset( $form_actions['entries'] );
					unset( $form_actions['duplicate'] );
					unset( $form_actions['trash'] );
				}
			}
		}

		return $form_actions;
	}
}



add_action( 'admin_head', 'init_ugf' );
/**
 * [init_ugf description]
 *
 * @return [type] [description]
 */
function init_ugf() {

	$screen  = get_current_screen();

	if ( $screen->id != 'toplevel_page_gf_edit_forms' ) {
		return;
	}

	$gf_contact_form = array (
		'title' => 'Test Contact',
		'fields' => array (
			array (
				'id' => 1,
				'label' => 'Name',
				'adminLabel' => '',
				'type' => 'name',
				'isRequired' => true,
				'inputs' => array (
					array (
						'id' => 1.3,
						'label' => 'First',
						'name' => '',
					),
					array (
						'id' => 1.6,
						'label' => 'Last',
						'name' => '',
					),
				),
			),
			array (
				'id' => 2,
				'label' => 'Email',
				'adminLabel' => '',
				'type' => 'email',
				'isRequired' => true,
			),
			array (
				'id' => 3,
				'label' => 'Subject',
				'adminLabel' => '',
				'type' => 'text',
				'isRequired' => true,
			),
			array (
				'id' => 4,
				'label' => 'Message',
				'adminLabel' => '',
				'type' => 'textarea',
				'isRequired' => true,
			),
		),
		'is_active' => '1',
		'date_created' => date( 'Y-m-d H:i:s' ),
	);


	$ugf = new ProgrammaticallyAddGravityForm();
	$ugf->SetForm( $gf_contact_form );

}