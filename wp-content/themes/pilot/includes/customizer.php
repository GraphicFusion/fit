<?php
/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function _s_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', '_s_customize_register' );

function get_footer_social_platforms() {

	// define social fields to store user settings
	return array(
		'facebook'
	,	'twitter'
	,	'pinterest'
	,	'instagram'
	,	'youtube'
	);

}

function get_footer_social_fieldname( $social_platform ) {
	return strtolower( $social_platform ) . '_textbox';
}

function get_footer_social_displayname( $social_platform ) {
	return ucfirst( $social_platform );
}

function sanitize_text( $input ) {
	return wp_kses_post( force_balance_tags( $input ) );
}

function add_footer_content($wp_customize) {

	// define new section for Footer
	$FOOTER_SECTION = 'footer';
	$wp_customize->add_section(
		$FOOTER_SECTION,
		array(
			'title' => 'Footer',
			'description' => 'Change the content of the footer. e.g. configure social platform links',
			'priority' => 135,
		)
	);

	// create a setting for each social platform defined in $social_fields
	$social_platforms = get_footer_social_platforms();
	foreach( $social_platforms as $social_platform ):

		$field_name = get_footer_social_fieldname($social_platform);
		$wp_customize->add_setting(
			$field_name,
			array(
				'sanitize_callback' => 'sanitize_text',
			)
		);

		$wp_customize->add_control(
			$field_name,
			array(
				'label' => get_footer_social_displayname($social_platform),
				'section' => $FOOTER_SECTION,
				'type' => 'text',
			)
		);

	endforeach;

} // add_footer_content()
add_action('customize_register', 'add_footer_content');


/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function _s_customize_preview_js() {
	wp_enqueue_script( '_s_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', '_s_customize_preview_js' );
