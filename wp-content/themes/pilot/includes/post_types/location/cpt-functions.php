<?php
	require get_template_directory() . '/includes/post_types/location/cpt-register.php';
	require get_template_directory() . '/includes/post_types/location/cpt-acf.php';

class Location{
	public function __construct( $post_ref ){
		if( is_numeric( $post_ref ) ){
			$post = get_post( $post_ref );
		}
		elseif( is_object( $post_ref ) ){
			$post = $post_ref;
		}
		if( is_object( $post ) ){
			foreach( $post as $key => $value ){
				$this->$key = $value;
			}
			$geo = get_field( 'location_address', $this->ID);
			$this->address = $geo['address'];
			$this->lat = $geo['lat'];
			$this->lng = $geo['lng'];
			$this->city = get_field( 'location_city', $this->ID);
			$this->state = get_field( 'location_state', $this->ID);
			$this->zip_code = get_field( 'location_zip_code', $this->ID);
		}
	}
}
function get_all_locations(){
	$locations = array();
	$args=array(
	  'post_type' => 'location',
	  'post_status' => 'publish',
	  'posts_per_page' => -1
	);
	$loc_query = new WP_Query($args);
	if( $loc_query->have_posts() ):
		foreach($loc_query->posts as $loc_post):
			$locations[] = new Location($loc_post);			
		endforeach;
	endif;
	return $locations;
}