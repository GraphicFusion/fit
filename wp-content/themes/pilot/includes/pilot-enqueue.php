<?php
/**
 * Enqueue scripts and styles.
 */
function pilot_scripts() {

	// Remove query var from assets if in production - avoids cacheing of css/js in development; improves speed test score in prod
	if( WP_ENV == 'production' ){
		$asset_var = null;
		add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
		add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );
	}
	else{
		$asset_var = time(); 
	}

    wp_enqueue_style( 'pilot-style', get_template_directory_uri().'/dest/css/main.min.css');

	wp_deregister_script('jquery');
	wp_register_script('jquery', ('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'), false, $asset_var, true);

	wp_enqueue_script( 'pilot-libraries', get_template_directory_uri() . '/dest/js/lib.min.js', array('jquery'), $asset_var, true );
	wp_enqueue_script( 'pilot-wow', get_template_directory_uri() . '/bower_components/wow/dist/wow.js', array('jquery'), $asset_var, false );
    wp_enqueue_script( 'pilot-scripts', get_template_directory_uri() . '/dest/js/app.min.js', array('jquery'), $asset_var, true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// enqueue styles and scripts
	wp_localize_script( 'sorcery-scripts', 'ajax', array('ajaxurl' => admin_url('admin-ajax.php'),) );

}
add_action( 'wp_enqueue_scripts', 'pilot_scripts' );

function _remove_script_version( $src ){
	$parts = explode( '?', $src );
	return $parts[0];
}

//enqueue CSS into admin
function load_custom_wp_admin_style() {
	wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/dest/css/icons.min.css', false, '1.0.0' );
	wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );