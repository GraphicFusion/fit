<footer class="site-footer">
	<div class="footer-inner">
		<?php get_template_part( 'views/site-brand' ); ?>

		<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'footer-menu' ) ); ?>

		<?php get_template_part( 'views/footer-social' ); ?>

		<div class="site-info">
			&copy; <?php echo date("Y");  ?>
			<?php echo bloginfo('name') ?>.
			<?php _e('All Rights Reserved.', 'pilot')  ?>
		</div><!-- .site-info -->
	</div>
</footer>

<nav id="site-navigation" class="main-navigation">
	<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'pilot' ); ?></button>
	<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
</nav>

<div id="menu-background"></div>

