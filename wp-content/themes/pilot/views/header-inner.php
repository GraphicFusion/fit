<header id="masthead" class="site-header inner-header <?php echo is_admin_bar_showing() ? 'admin-bar-showing' : '' ?>">
    <div class="site-header-wrap">
        <div class="header-left col-4">
            <div class="header-logo">
                <a href="/"><img src="<?php echo get_theme_mod('header_logo', ''); ?>"/></a>
            </div>
        </div>
        <div class="header-right col-8 col-last">
            <nav id="site-navigation" class="main-navigation">
                <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu')); ?>
                <?php get_search_form(true); ?>
            </nav>

        </div>
    </div>
</header>