<div class="contact-info-block">
    <ul class="contact-info-social-list">
        <?php

            foreach(get_footer_social_platforms() as $platform_name ):

                $platform_displayname = get_footer_social_displayname($platform_name);
                $platform_fieldname = get_footer_social_fieldname($platform_name);
                $customizer_value = get_theme_mod($platform_fieldname);
                if( !empty($customizer_value) ):
                ?>

                <li class="contact-info-social-list-item">
                    <a href="[<?php echo $customizer_value ?>]"><?php get_template_part('views/icons/' . $platform_name); ?></a>
                </li>

                <?php
                endif;
            endforeach;

        ?>
<!--        <li class="contact-info-social-list-item">-->
<!--            <a href="">--><?php //get_template_part('views/icons/facebook'); ?><!--</a>-->
<!--        </li>-->
<!--        <li class="contact-info-social-list-item">-->
<!--            <a href="">--><?php //get_template_part('views/icons/twitter'); ?><!--</a>-->
<!--        </li>-->
<!--        <li class="contact-info-social-list-item">-->
<!--            <a href="">--><?php //get_template_part('views/icons/pinterest'); ?><!--</a>-->
<!--        </li>-->
<!--        <li class="contact-info-social-list-item">-->
<!--            <a href="">--><?php //get_template_part('views/icons/instagram'); ?><!--</a>-->
<!--        </li>-->
<!--        <li class="contact-info-social-list-item">-->
<!--            <a href="">--><?php //get_template_part('views/icons/youtube'); ?><!--</a>-->
<!--        </li>-->
    </ul>
</div>

