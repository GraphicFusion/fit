<header id="masthead" class="site-header">
	<?php get_template_part( 'views/site-brand' ); ?>

	<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'pilot' ); ?></button>
</header>

