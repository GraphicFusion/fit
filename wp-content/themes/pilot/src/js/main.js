(function() {

    //wow = new WOW({
    //    boxClass:     'wow',      // default
    //    animateClass: 'animated', // default
    //    offset:       0,          // default
    //    mobile:       true,       // default
    //    live:         true        // default
    //});
    //wow.init();

})();
jQuery(document).ready(function($) {

    // toggle main menu
    var $menuToggle = $('.menu-toggle')
    ,   $mainMenuBg = $('#menu-background')
    ,   $mainNav = $('.main-navigation')
    ,   $body = $('body')
    ,   is_active = 'is-active';

    $menuToggle.add($mainMenuBg).click(function(){

        $body.toggleClass(is_active).scrollTop(0);
        $mainNav.toggleClass(is_active);
        $mainMenuBg.toggleClass(is_active);
        $menuToggle.toggleClass(is_active);

    })
});