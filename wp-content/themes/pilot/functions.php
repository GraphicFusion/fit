<?php

function getPilotIcons()
{
    return array(
        'icon-stopwatch' => 'Stopwatch',
        'icon-dumbell' => 'Dumbbell',
        'icon-bike' => 'Bike',
        'icon-trophy' => 'Trophy',
        'icon-measuring-tap' => 'Measuring Tape',
        'icon-bullseye' => 'Bullseye',
        'icon-podium' => 'Podium',
        'icon-football' => 'Football',
        'icon-compass' => 'Compass',
        'icon-circle-symbol' => 'Circle Symbol',
        'icon-clipboard' => 'Clipboard',
        'icon-watch' => 'Watch',
        'icon-heart' => 'Heart',
        'icon-scale' => 'Scale',
        'icon-hand-grid' => 'Hand Grid',
        'icon-tshirt' => 'T-Shirt',
        'icon-weight-loss' => 'Weight Loss',
        'icon-toggle' => 'Toggle',
        'icon-jump-rope' => 'Jump Rope',
        'icon-basketball' => 'Basketball',
        'icon-run' => 'Run',
        'icon-prescription' => 'Prescription',
        'icon-medicine-bottle-1' => 'Medicine Bottle 1',
        'icon-medicine-bottle-2' => 'Medicine Bottle 2',
        'icon-fitness-badge' => 'Fitness Badge',
        'icon-jog' => 'Jog',
        'icon-rx' => 'Rx Symbol',
        'icon-stethiscope' => 'Stethoscope',
        'icon-pool' => 'Pool',
        'icon-tabs' => 'Tabs',
        'icon-lungs' => 'Lungs',
        'icon-weights' => 'Weights',
        'icon-biceps' => 'Biceps',
        'icon-bench-1' => 'Bench 1',
        'icon-bench-2' => 'Bench 2',
        'icon-curl' => 'Curl',
        'icon-lunge' => 'Lunge',
        'icon-dumbell-2' => 'Dumbbell 2',
        'icon-star' => 'Star',
        'icon-medicine-bottle-3' => 'Medicine Bottle 3',
        'icon-no-smoking' => 'No Smoking',
        'icon-cherry' => 'Cherry',
        'icon-beakers' => 'Beakers',
        'icon-therm' => 'Thermometer',
        'icon-basketball-court' => 'Basketball Court',
        'icon-golf' => 'Golf',
        'icon-pill' => 'Pill',
        'icon-treadmill' => 'Treadmill',
        'icon-exercise-bike' => 'Excerise Bike',
        'icon-fish' => 'Fish',
        'icon-ekg' => 'EKG Machine',
        'icon-dumbell-3' => 'Dumbbell 3',
        'icon-sprint' => 'Sprint',
        'icon-beer' => 'Beer',
        'icon-lightning' => 'Lightning',
        'icon-apple' => 'Apple',
        'icon-pills' => 'Pills',
        'icon-gymnastics' => 'Gymnastics',
        'icon-cricket' => 'Cricket',
        'icon-sign' => 'Sign',
        'icon-coffee' => 'Coffee',
        'icon-kettle' => 'Kettle Bell',
        'icon-doctor-bag' => 'Doctor Bag',
        'icon-fist' => 'Fist',
        'icon-sunny' => 'Sunny',
        'icon-vitamins' => 'Vitamins',
        'icon-muscles' => 'Muscels',
        'icon-yoga' => 'Yoga',
        'icon-water-bottle' => 'Water Bottle',
        'icon-weight-machine' => 'Weight Machine',
        'icon-medal' => 'Medal',
        'icon-bench-press' => 'Bench Press',
        'icon-no-alcohol' => 'No Alcohol',
        'icon-kidneys' => 'Kidneys',
        'icon-tea' => 'Tea',
        'icon-medicine-bottle-4' => 'Medicine Bottle 4',
        'icon-chart' => 'Chart',
        'icon-flask' => 'Flask',
        'icon-puzzle' => 'Puzzle',
        'icon-pills-2' => 'Pills 2',
        'icon-medicine-bag' => 'Medicine bag',
        'icon-heart-chart' => 'Heart Chart',
        'icon-medicine-bottle-5' => 'Medicine Bottle 5',
        'icon-no-smoking-2' => 'No Smoking 2',
        'icon-band-aid' => 'Band Aid',
        'icon-music' => 'Music',
        'icon-carafe' => 'Carafe',
        'icon-expand' => 'Expand',
        'icon-barbell' => 'Barbell',
        'icon-sign-2' => 'Sign 2',
        'icon-apple-document' => 'Apple Document',
        'icon-pencil' => 'Pencil',
        'icon-boxing' => 'Boxing',
        'icon-yin-yang' => 'Yin Yang',
        'icon-lego' => 'Lego',
        'icon-puzzle-2' => 'Puzzle 2',
        'icon-apple-watch' => 'Apple Watch',
        'icon-calendar' => 'Calendar',
        'icon-carrot' => 'Carrot',
        'icon-treadmill-2' => 'Treadmill 2'
    );
}

/*
 * Global settings for Pilot Theme
 * The global $pilot object is defined in includes/pilot.class.php
 *
 * Are you adding a new function for the theme?
 *		If you don't know where it should go, put it in includes/pilot-theme-functions.php
 *
 *		If it's part of the core of the theme management, place it as a Method in includes/pilot.class.php
 *
 **/
require get_template_directory() . '/includes/pilot.class.php';
$args = array(
    'sidebar' => 0,                // set to 1 to use sidebar
    'comments' => 0,                // set to 1 to use comments
    'default_admin' => 1,                // set to 0 to remove extraneous meta boxes in admin
    'use_colormaker' => 1,                // set to 1 to use theme-wide color-maker plugin - not yet available
    'add_acf_options_pages' => 1,                // set to 1 to add options pages
    'use_default_page_titles' => 1,                // set to 1 to use default page titles (adds a hide option for indiv pages); 0 to not use default page titles;
    'include_modules' => 1,                // set to 1 to use modules
    'use_global_modules' => 1,                // set to 1 to use global modules
    'module_classes' => " module ",        // a string of classes that will be added to every module wrapper. i.e., ' wow fadeInUP';
    'additional_modules' => array(),            // add modules that may have failed to be included automatically
    'use_environments' => 1,                // set to 1 to use autodetect of environments
    'environments' => array(
        'development' => array(                        // array of development (local) environment urls
            'fittheme.dev'
        ),
        'staging' => array(                            // array of staging (staging server) environment urls
            'fittheme.wpengine.com'
        ),
        'production' => array(                        // array of staging (staging server) environment urls
            'fittheme.wpengine.com'
        )
    )
);
global $pilot;
$pilot = new Pilot($args);
$pilot->build_pilot();                        // runs after object is created; checks theme settings and requires conditionally